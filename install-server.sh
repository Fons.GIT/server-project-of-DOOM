#!/bin/sh -eu

# Config
forge_installer_url=http://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.4.1614-1.7.10/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar
forge_installer_jar=forge-1.7.10-10.13.4.1614-1.7.10-installer.jar

# Get server
curl "$forge_installer_url" > "$forge_installer_jar"

# Install server
java -jar "$forge_installer_jar" --installServer

# Clean up
rm "$forge_installer_jar"
rm "${forge_installer_jar}.log"
